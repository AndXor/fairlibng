package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.ExchangeApi;
import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.entities.EventTypeResult;
import org.bitbucket.komarevsky.fairlibng.entities.MarketBook;
import org.bitbucket.komarevsky.fairlibng.entities.MarketCatalogue;
import org.bitbucket.komarevsky.fairlibng.entities.PlaceExecutionReport;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ExchangeApiRescript extends ExchangeApi {

    public ExchangeApiRescript(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
        protocolType = ProtocolType.RESCRIPT;
    }

    @Override
    protected Type getContainerTypeForListEventTypes() {
        return new TypeToken<List<EventTypeResult>>() {}.getType();
    }

    @Override
    protected Type getContainerTypeForListMarketBook() {
        return new TypeToken<List<MarketBook>>() {}.getType();
    }

    @Override
    protected Type getContainerTypeForListMarketCatalogue() {
        return new TypeToken<List<MarketCatalogue>>() {}.getType();
    }

    @Override
    protected Type getContainerTypeForPlaceOrders() {
        return new TypeToken<PlaceExecutionReport>() {}.getType();
    }
}
