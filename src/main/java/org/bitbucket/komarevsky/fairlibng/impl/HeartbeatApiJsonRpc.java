package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.HeartbeatApi;
import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.entities.HeartbeatReport;
import org.bitbucket.komarevsky.fairlibng.internal.model.rpc.Container;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class HeartbeatApiJsonRpc extends HeartbeatApi {

    public HeartbeatApiJsonRpc(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
        protocolType = ProtocolType.JSON_RPC;
    }

    @Override
    protected Type getContainerTypeForHeartbeat() {
        return new TypeToken<Container<HeartbeatReport>>() {}.getType();
    }
}
