package org.bitbucket.komarevsky.fairlibng;

import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.BetfairApi;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolConstant;
import org.bitbucket.komarevsky.fairlibng.entities.AccountFundsResponse;
import org.bitbucket.komarevsky.fairlibng.internal.operation.ApiNgOperation;
import org.bitbucket.komarevsky.fairlibng.enums.Wallet;
import org.bitbucket.komarevsky.fairlibng.exceptions.APINGException;
import org.bitbucket.komarevsky.fairlibng.internal.response.ResponseConverter;

import java.lang.reflect.Type;

public abstract class AccountsApi extends BetfairApi {

    protected static final String WALLET = "wallet";

    public AccountsApi(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
    }

    public AccountFundsResponse getAccountFunds(Wallet wallet) throws APINGException {
        String operationName = getOperation(ApiNgOperation.GETACCOUNTFUNDS);
        String result = getRequestBuilder()
                .withOperation(operationName)
                .withParam(WALLET, wallet)
                .build();

        Type containerType = getContainerTypeForGetAccountFunds();
        ResponseConverter<AccountFundsResponse> responseConverter = new ResponseConverter<>();
        responseConverter.setProtocolType(protocolType);
        responseConverter.setResponse(result);
        responseConverter.setContainerType(containerType);
        return responseConverter.convert();
    }

    @Override
    protected String getEndpoint() {
        return connectionConfiguration.getAccountsApiUrl();
    }

    @Override
    protected String getJsonRpcMethodPrefix() {
        return ProtocolConstant.JSON_RPC_ACCOUNTS_METHOD_PREFIX;
    }

    protected abstract Type getContainerTypeForGetAccountFunds();
}
