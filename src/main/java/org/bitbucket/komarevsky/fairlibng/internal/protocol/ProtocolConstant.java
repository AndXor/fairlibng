package org.bitbucket.komarevsky.fairlibng.internal.protocol;

public class ProtocolConstant {
    public static final String RESCRIPT_SUFFIX = "rest/v1.0/";

    public static final String JSON_RPC_SUFFIX = "json-rpc/v1";
    public static final String JSON_RPC_EXCHANGE_METHOD_PREFIX = "SportsAPING/v1.0/";
    public static final String JSON_RPC_ACCOUNTS_METHOD_PREFIX = "AccountAPING/v1.0/";
    public static final String JSON_RPC_HEARTBEAT_METHOD_PREFIX = "HeartbeatAPING/v1.0/";

    private ProtocolConstant() {}
}
