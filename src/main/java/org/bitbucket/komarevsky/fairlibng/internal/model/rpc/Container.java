package org.bitbucket.komarevsky.fairlibng.internal.model.rpc;

public class Container<T> {
	
	private Error error;
	private String jsonrpc;
	private T result;
	
	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

    public String getJsonrpc() {
		return jsonrpc;
	}

    public void setJsonrpc(String jsonrpc) {
		this.jsonrpc = jsonrpc;
	}

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
