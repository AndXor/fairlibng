package org.bitbucket.komarevsky.fairlibng.internal.request;

import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolConstant;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.exceptions.APINGException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.bitbucket.komarevsky.fairlibng.internal.response.JsonResponseHandler;
import org.bitbucket.komarevsky.fairlibng.internal.response.RescriptResponseHandler;
import org.bitbucket.komarevsky.fairlibng.internal.json.JsonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Documentation
 */
public class Requester {

    private static final Logger LOGGER = LoggerFactory.getLogger(Requester.class);

    private static final String HTTP_HEADER_X_APPLICATION = "X-Application";
    private static final String HTTP_HEADER_X_AUTHENTICATION = "X-Authentication";
    private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HTTP_HEADER_ACCEPT = "Accept";
    private static final String HTTP_HEADER_ACCEPT_CHARSET = "Accept-Charset";

    private static final String ENCODING_UTF8="UTF-8";
    private static final String APPLICATION_JSON="application/json";

    private ProtocolType protocolType;
    private String operation;
    private Map<String, Object> params;
    private String endpoint;
    private String timeout;
    private String appKey;
    private String token;

    public void setProtocolType(ProtocolType protocolType) {
        this.protocolType = protocolType;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String makeRequest() {
        String result;

        switch (protocolType) {
            case JSON_RPC :
                result = makeJsonRpcRequest();
                break;
            case RESCRIPT:
                result = makeRescriptRequest();
                break;
            case DIRECT_LINK:
                result = makeDirectLinkRequest();
                break;
            default:
                throw new UnsupportedOperationException();
        }

        LOGGER.debug("Response: {}", result);
        return result;
    }

    protected String makeJsonRpcRequest() {
        //Handling the JSON-RPC request
        JsonrpcRequest request = new JsonrpcRequest();
        request.setId("1");
        request.setMethod(operation);
        request.setParams(params);

        String requestString = JsonConverter.convertToJson(request);
        LOGGER.debug("Request string: {}", requestString);

        String apiNgURL = endpoint + ProtocolConstant.JSON_RPC_SUFFIX;
        return sendPostRequest(requestString, apiNgURL, new JsonResponseHandler());
    }

    protected String makeRescriptRequest() throws APINGException {
        String requestString;
        //Handling the Rescript request
        params.put("id", 1);

        requestString =  JsonConverter.convertToJson(params);
        LOGGER.debug("Request string: {}", requestString);

        String apiNgURL = endpoint + ProtocolConstant.RESCRIPT_SUFFIX + operation + "/";
        String response = sendPostRequest(requestString, apiNgURL, new RescriptResponseHandler());
        if(response != null) {
            return response;
        } else {
            throw new APINGException();
        }
    }

    protected String makeDirectLinkRequest() throws APINGException {
        String apiNgURL = endpoint + operation;
        LOGGER.debug("Request url: {}", apiNgURL);
        String response = sendPostRequest("", apiNgURL, new RescriptResponseHandler());
        if(response != null) {
            return response;
        } else {
            throw new APINGException();
        }
    }

    protected String sendPostRequest(String jsonRequest, String URL, ResponseHandler<String> reqHandler){
        HttpPost post = new HttpPost(URL);
        String resp = null;
        try {
            post.setHeader(HTTP_HEADER_CONTENT_TYPE, APPLICATION_JSON);
            post.setHeader(HTTP_HEADER_ACCEPT, APPLICATION_JSON);
            post.setHeader(HTTP_HEADER_ACCEPT_CHARSET, ENCODING_UTF8);
            post.setHeader(HTTP_HEADER_X_APPLICATION, appKey);
            post.setHeader(HTTP_HEADER_X_AUTHENTICATION, token);

            post.setEntity(new StringEntity(jsonRequest, ENCODING_UTF8));

            HttpClient httpClient = new DefaultHttpClient();

            HttpParams httpParams = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, new Integer(timeout).intValue());
            HttpConnectionParams.setSoTimeout(httpParams, new Integer(timeout).intValue());

            resp = httpClient.execute(post, reqHandler);

        } catch (UnsupportedEncodingException e1) {
            LOGGER.error("unsupported encoding " + ENCODING_UTF8, e1);
        } catch (ClientProtocolException e) {
            LOGGER.error("protocol error", e);
        } catch (IOException ioE){
            LOGGER.error("IO exception", ioE);
        }

        return resp;
    }
}
