package org.bitbucket.komarevsky.fairlibng.internal;

import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;
import org.bitbucket.komarevsky.fairlibng.internal.operation.ApiNgOperation;
import org.bitbucket.komarevsky.fairlibng.internal.request.RequestBuilder;

import java.util.Locale;

import static org.bitbucket.komarevsky.fairlibng.internal.request.RequestBuilder.requestBuilder;

public abstract class BetfairApi {

    protected String locale = Locale.getDefault().toString();

    protected ConnectionConfiguration connectionConfiguration;
    protected ProtocolType protocolType;

    public BetfairApi(ConnectionConfiguration connectionConfiguration) {
        this.connectionConfiguration = connectionConfiguration;
    }

    public void setConnectionConfiguration(ConnectionConfiguration connectionConfiguration) {
        this.connectionConfiguration = connectionConfiguration;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public ProtocolType getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(ProtocolType protocolType) {
        this.protocolType = protocolType;
    }

    protected RequestBuilder getRequestBuilder() {
        return requestBuilder()
                .withProtocolType(protocolType)
                .withEndpoint(getEndpoint())
                .withTimeout(connectionConfiguration.getTimeout())
                .withAppKey(connectionConfiguration.getApplicationKey())
                .withToken(connectionConfiguration.getSessionToken());
    }

    protected String getOperation(ApiNgOperation apiNgOperation) {
        switch (protocolType) {
            case JSON_RPC :
                return getJsonRpcMethodPrefix() + apiNgOperation.getOperationName();
            case RESCRIPT:
                return apiNgOperation.getOperationName();
            case DIRECT_LINK:
                return apiNgOperation.getOperationName();
            default:
                throw new UnsupportedOperationException();
        }
    }

    protected abstract String getJsonRpcMethodPrefix();

    protected abstract String getEndpoint();
}
