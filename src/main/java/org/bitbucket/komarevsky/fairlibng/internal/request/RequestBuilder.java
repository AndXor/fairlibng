package org.bitbucket.komarevsky.fairlibng.internal.request;

import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolType;

import java.util.HashMap;
import java.util.Map;

public class RequestBuilder {

    private ProtocolType protocolType;
    private String operation;
    private Map<String, Object> params = new HashMap<>();
    private String endpoint;
    private String timeout;
    private String appKey;
    private String token;

    public static RequestBuilder requestBuilder() {
        return new RequestBuilder();
    }

    public RequestBuilder withProtocolType(ProtocolType protocolType) {
        this.protocolType = protocolType;
        return this;
    }

    public RequestBuilder withOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public RequestBuilder withParam(String key, Object value) {
        this.params.put(key, value);
        return this;
    }

    public RequestBuilder withEndpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public RequestBuilder withTimeout(String timeout) {
        this.timeout = timeout;
        return this;
    }

    public RequestBuilder withAppKey(String appKey) {
        this.appKey = appKey;
        return this;
    }

    public RequestBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public String build() {
        Requester requester = new Requester();

        requester.setAppKey(appKey);
        requester.setEndpoint(endpoint);
        requester.setOperation(operation);
        requester.setParams(params);
        requester.setProtocolType(protocolType);
        requester.setTimeout(timeout);
        requester.setToken(token);

        return requester.makeRequest();
    }

    private RequestBuilder() {}
}
