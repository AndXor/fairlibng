package org.bitbucket.komarevsky.fairlibng.internal.model.rpc;

import org.bitbucket.komarevsky.fairlibng.exceptions.APINGException;

public class Data {

	private APINGException APINGException;

	public APINGException getAPINGException() {
		return APINGException;
	}

	public void setAPINGException(APINGException aPINGException) {
		APINGException = aPINGException;
	}

}
