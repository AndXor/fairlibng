package org.bitbucket.komarevsky.fairlibng;

import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.internal.BetfairApi;
import org.bitbucket.komarevsky.fairlibng.internal.protocol.ProtocolConstant;
import org.bitbucket.komarevsky.fairlibng.entities.EventTypeResult;
import org.bitbucket.komarevsky.fairlibng.entities.MarketBook;
import org.bitbucket.komarevsky.fairlibng.entities.MarketCatalogue;
import org.bitbucket.komarevsky.fairlibng.entities.MarketFilter;
import org.bitbucket.komarevsky.fairlibng.entities.PlaceExecutionReport;
import org.bitbucket.komarevsky.fairlibng.entities.PlaceInstruction;
import org.bitbucket.komarevsky.fairlibng.entities.PriceProjection;
import org.bitbucket.komarevsky.fairlibng.internal.operation.ApiNgOperation;
import org.bitbucket.komarevsky.fairlibng.enums.MarketProjection;
import org.bitbucket.komarevsky.fairlibng.enums.MarketSort;
import org.bitbucket.komarevsky.fairlibng.enums.MatchProjection;
import org.bitbucket.komarevsky.fairlibng.enums.OrderProjection;
import org.bitbucket.komarevsky.fairlibng.exceptions.APINGException;
import org.bitbucket.komarevsky.fairlibng.internal.response.ResponseConverter;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Set;

public abstract class ExchangeApi extends BetfairApi {
	protected final String FILTER = "filter";
    protected final String LOCALE = "locale";
    protected final String SORT = "sort";
    protected final String MAX_RESULT = "maxResults";
    protected final String MARKET_IDS = "marketIds";
    protected final String MARKET_ID = "marketId";
    protected final String INSTRUCTIONS = "instructions";
    protected final String CUSTOMER_REF = "customerRef";
    protected final String MARKET_PROJECTION = "marketProjection";
    protected final String PRICE_PROJECTION = "priceProjection";
    protected final String MATCH_PROJECTION = "matchProjection";
    protected final String ORDER_PROJECTION = "orderProjection";
    protected final String CURRENCY_CODE = "currencyCode";

    public ExchangeApi(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
    }

    public List<EventTypeResult> listEventTypes(MarketFilter filter) throws APINGException {
        String operationName = getOperation(ApiNgOperation.LISTEVENTTYPES);
        String result = getRequestBuilder()
                .withOperation(operationName)
                .withParam(FILTER, filter)
                .withParam(LOCALE, locale)
                .build();

        Type containerType = getContainerTypeForListEventTypes();
        ResponseConverter<List<EventTypeResult>> responseConverter = new ResponseConverter<>();
        responseConverter.setProtocolType(protocolType);
        responseConverter.setResponse(result);
        responseConverter.setContainerType(containerType);
        return responseConverter.convert();
    }

    public List<MarketBook> listMarketBook(List<String> marketIds, PriceProjection priceProjection, OrderProjection orderProjection,
                                           MatchProjection matchProjection, String currencyCode) throws APINGException {
        String operationName = getOperation(ApiNgOperation.LISTMARKETBOOK);
        String result = getRequestBuilder()
                .withOperation(operationName)
                .withParam(LOCALE, locale)
                .withParam(MARKET_IDS, marketIds)
                .withParam(PRICE_PROJECTION, priceProjection)
                .withParam(ORDER_PROJECTION, orderProjection)
                .withParam(MATCH_PROJECTION, matchProjection)
                .withParam(CURRENCY_CODE, currencyCode)
                .build();

        Type containerType = getContainerTypeForListMarketBook();
        ResponseConverter<List<MarketBook>> responseConverter = new ResponseConverter<>();
        responseConverter.setProtocolType(protocolType);
        responseConverter.setResponse(result);
        responseConverter.setContainerType(containerType);
        return responseConverter.convert();
    }

    public List<MarketCatalogue> listMarketCatalogue(MarketFilter filter, Set<MarketProjection> marketProjection,
                                                     MarketSort sort, String maxResult) throws APINGException {
        String operationName = getOperation(ApiNgOperation.LISTMARKETCATALOGUE);
        String result = getRequestBuilder()
                .withOperation(operationName)
                .withParam(LOCALE, locale)
                .withParam(FILTER, filter)
                .withParam(SORT, sort)
                .withParam(MAX_RESULT, maxResult)
                .withParam(MARKET_PROJECTION, marketProjection)
                .build();

        Type containerType = getContainerTypeForListMarketCatalogue();
        ResponseConverter<List<MarketCatalogue>> responseConverter = new ResponseConverter<>();
        responseConverter.setProtocolType(protocolType);
        responseConverter.setResponse(result);
        responseConverter.setContainerType(containerType);
        return responseConverter.convert();
    }

    public PlaceExecutionReport placeOrders(String marketId, List<PlaceInstruction> instructions, String customerRef) throws APINGException {
        String operationName = getOperation(ApiNgOperation.PLACORDERS);
        String result = getRequestBuilder()
                .withOperation(operationName)
                .withParam(LOCALE, locale)
                .withParam(MARKET_ID, marketId)
                .withParam(INSTRUCTIONS, instructions)
                .withParam(CUSTOMER_REF, customerRef)
                .build();

        Type containerType = getContainerTypeForPlaceOrders();
        ResponseConverter<PlaceExecutionReport> responseConverter = new ResponseConverter<>();
        responseConverter.setProtocolType(protocolType);
        responseConverter.setResponse(result);
        responseConverter.setContainerType(containerType);
        return responseConverter.convert();
    }

    @Override
    protected String getEndpoint() {
        return connectionConfiguration.getExchangeApiUrl();
    }

    @Override
    protected String getJsonRpcMethodPrefix() {
        return ProtocolConstant.JSON_RPC_EXCHANGE_METHOD_PREFIX;
    }

    protected abstract Type getContainerTypeForListEventTypes();
    protected abstract Type getContainerTypeForListMarketBook();
    protected abstract Type getContainerTypeForListMarketCatalogue();
    protected abstract Type getContainerTypeForPlaceOrders();
}

