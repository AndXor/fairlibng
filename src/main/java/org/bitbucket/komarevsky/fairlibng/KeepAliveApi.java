package org.bitbucket.komarevsky.fairlibng;

import org.bitbucket.komarevsky.fairlibng.configuration.ConnectionConfiguration;
import org.bitbucket.komarevsky.fairlibng.entities.KeepAliveResponse;
import org.bitbucket.komarevsky.fairlibng.internal.BetfairApi;
import org.bitbucket.komarevsky.fairlibng.internal.operation.ApiNgOperation;
import org.bitbucket.komarevsky.fairlibng.exceptions.APINGException;
import org.bitbucket.komarevsky.fairlibng.internal.response.ResponseConverter;

import java.lang.reflect.Type;

public abstract class KeepAliveApi extends BetfairApi {

    public KeepAliveApi(ConnectionConfiguration connectionConfiguration) {
        super(connectionConfiguration);
    }

    public KeepAliveResponse keepAlive() throws APINGException {
        String operationName = getOperation(ApiNgOperation.KEEPALIVE);
        String result = getRequestBuilder()
                .withOperation(operationName)
                .build();

        Type containerType = getContainerTypeForKeepAlive();
        ResponseConverter<KeepAliveResponse> responseConverter = new ResponseConverter<>();
        responseConverter.setProtocolType(protocolType);
        responseConverter.setResponse(result);
        responseConverter.setContainerType(containerType);
        return responseConverter.convert();
    }

    @Override
    protected String getEndpoint() {
        return connectionConfiguration.getKeepAliveApiUrl();
    }

    @Override
    protected String getJsonRpcMethodPrefix() {
        throw new UnsupportedOperationException();
    }

    protected abstract Type getContainerTypeForKeepAlive();
}
