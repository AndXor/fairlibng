package org.bitbucket.komarevsky.fairlibng.price;

import org.bitbucket.komarevsky.fairlibng.internal.model.PriceRule;

import java.util.List;

/**
 * Price for asian handicap and total goal markets
 */
public class AsianHandicapPrice extends AbstractPrice {

    private static final double MIN_PRICE = 1.01;
    private static final double MAX_PRICE = 1000;

    // list of rules for every possible interval
    private static final List<PriceRule> ruleList;

    //initialization of ruleList
    static {
        // array of [FROM_PRICE, TO_PRICE, INCREMENT]
        double[][] betfairRules = {
                {MIN_PRICE, MAX_PRICE, 0.01}
        };

        ruleList = getPriceRuleListByRulesArray(betfairRules);
    }

    public AsianHandicapPrice(double price) {
        super(price);
    }

    @Override
    public double getMaxPrice() {
        return MAX_PRICE;
    }

    @Override
    public double getMinPrice() {
        return MIN_PRICE;
    }

    @Override
    protected List<PriceRule> getPriceRuleList() {
        return ruleList;
    }

}
