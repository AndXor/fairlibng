package org.bitbucket.komarevsky.fairlibng.configuration.endpoint;

/**
 * addresses are taken from
 * https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Betting+API
 */
public enum ExchangeEndpoint {
    INTERNATIONAL("https://api.betfair.com/exchange/betting/"),
    AUSTRALIAN("https://api-au.betfair.com/exchange/betting/");

    private String endpoint;

    ExchangeEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
