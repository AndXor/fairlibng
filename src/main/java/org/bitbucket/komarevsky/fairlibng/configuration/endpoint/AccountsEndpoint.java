package org.bitbucket.komarevsky.fairlibng.configuration.endpoint;

/**
 * addresses are taken from:
 * https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Accounts+API
 */
public enum AccountsEndpoint {
    INTERNATIONAL("https://api.betfair.com/exchange/account/"),
    AUSTRALIAN("https://api-au.betfair.com/exchange/account/");

    private String endpoint;

    AccountsEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
