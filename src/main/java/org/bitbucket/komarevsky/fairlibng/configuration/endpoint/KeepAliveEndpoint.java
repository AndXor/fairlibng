package org.bitbucket.komarevsky.fairlibng.configuration.endpoint;

/**
 * addresses are taken from:
 * https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Keep+Alive
 */
public enum KeepAliveEndpoint {

    INTERNATIONAL("https://identitysso.betfair.com/api/"),
    ITALIAN("https://identitysso.betfair.it/api/");

    private String endpoint;

    KeepAliveEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
