package org.bitbucket.komarevsky.fairlibng.configuration.endpoint;

/**
 * addresses are taken from:
 * https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Heartbeat+API
 */
public enum HeartbeatEndpoint {

    INTERNATIONAL("https://api.betfair.com/exchange/heartbeat/"),
    AUSTRALIAN("https://api-au.betfair.com/exchange/heartbeat/");

    private String endpoint;

    HeartbeatEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpoint() {
        return endpoint;
    }
}
