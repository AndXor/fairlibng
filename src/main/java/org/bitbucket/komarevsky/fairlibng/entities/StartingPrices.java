package org.bitbucket.komarevsky.fairlibng.entities;

import java.util.List;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class StartingPrices {
	private Double nearPrice;
	private Double farPrice;
	private List<PriceSize> backStakeTaken = null;
	private List<PriceSize> layLiabilityTaken = null;
	private Double actualSP;

	public Double getNearPrice() {
		return nearPrice;
	}

	public void setNearPrice(Double nearPrice) {
		this.nearPrice = nearPrice;
	}

	public Double getFarPrice() {
		return farPrice;
	}

	public void setFarPrice(Double farPrice) {
		this.farPrice = farPrice;
	}

	public List<PriceSize> getBackStakeTaken() {
		return backStakeTaken;
	}

	public void setBackStakeTaken(List<PriceSize> backStakeTaken) {
		this.backStakeTaken = backStakeTaken;
	}

	public List<PriceSize> getLayLiabilityTaken() {
		return layLiabilityTaken;
	}

	public void setLayLiabilityTaken(List<PriceSize> layLiabilityTaken) {
		this.layLiabilityTaken = layLiabilityTaken;
	}

	public Double getActualSP() {
		return actualSP;
	}

	public void setActualSP(Double actualSP) {
		this.actualSP = actualSP;
	}

	@Override
	public int hashCode() {
		return reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return reflectionToString(this);
	}
}
