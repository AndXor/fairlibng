package org.bitbucket.komarevsky.fairlibng.entities;

import org.apache.commons.lang3.StringUtils;
import org.bitbucket.komarevsky.fairlibng.enums.KeepAliveError;
import org.bitbucket.komarevsky.fairlibng.enums.KeepAliveStatus;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

/**
 * see https://api.developer.betfair.com/services/webapps/docs/display/1smk3cen4v3lu3yomq5qye0ni/Keep+Alive#KeepAlive-Responsestructure
 */
public class KeepAliveResponse {

    private String token;
    private String product;
    private KeepAliveStatus status;
    private KeepAliveError error;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public KeepAliveStatus getStatus() {
        return status;
    }

    public void setStatus(KeepAliveStatus status) {
        this.status = status;
    }

    public KeepAliveError getError() {
        return error;
    }

    public void setError(KeepAliveError error) {
        this.error = error;
    }

    @Override
    public boolean equals(Object o) {
        return reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return "KeepAliveResponse{" +
                "token is " + (StringUtils.isNotEmpty(token) ? "not" : "") + " empty" +
                ", product is" + (StringUtils.isNotEmpty(product) ? "not" : "") + " empty" +
                ", status=" + status +
                ", error=" + error +
                '}';
    }
}
