package org.bitbucket.komarevsky.fairlibng.entities;

import org.bitbucket.komarevsky.fairlibng.enums.RollupModel;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

public class ExBestOfferOverRides {

	private int bestPricesDepth;
	private RollupModel rollupModel;
	private int rollupLimit;
	private Double rollupLiabilityThreshold;
	private int rollupLiabilityFactor;

	public int getBestPricesDepth() {
		return bestPricesDepth;
	}

	public void setBestPricesDepth(int bestPricesDepth) {
		this.bestPricesDepth = bestPricesDepth;
	}

	public RollupModel getRollupModel() {
		return rollupModel;
	}

	public void setRollupModel(RollupModel rollupModel) {
		this.rollupModel = rollupModel;
	}

	public int getRollupLimit() {
		return rollupLimit;
	}

	public void setRollupLimit(int rollupLimit) {
		this.rollupLimit = rollupLimit;
	}

	public Double getRollupLiabilityThreshold() {
		return rollupLiabilityThreshold;
	}

	public void setRollupLiabilityThreshold(Double rollupLiabilityThreshold) {
		this.rollupLiabilityThreshold = rollupLiabilityThreshold;
	}

	public int getRollupLiabilityFactor() {
		return rollupLiabilityFactor;
	}

	public void setRollupLiabilityFactor(int rollupLiabilityFactor) {
		this.rollupLiabilityFactor = rollupLiabilityFactor;
	}

	@Override
	public int hashCode() {
		return reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return reflectionToString(this);
	}

}
