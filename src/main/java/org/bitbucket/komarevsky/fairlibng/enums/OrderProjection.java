package org.bitbucket.komarevsky.fairlibng.enums;

public enum OrderProjection {
	ALL, EXECUTABLE, EXECUTION_COMPLETE;
}
