package org.bitbucket.komarevsky.fairlibng.enums;

public enum InstructionReportStatus {
	SUCCESS, FAILURE, TIMEOUT;
}
