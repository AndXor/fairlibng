package org.bitbucket.komarevsky.fairlibng.impl;

import org.bitbucket.komarevsky.fairlibng.entities.EventTypeResult;
import org.bitbucket.komarevsky.fairlibng.entities.MarketFilter;
import org.bitbucket.komarevsky.fairlibng.testutil.BetfairAbstractIT;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class ExchangeApiRescriptTestIT extends BetfairAbstractIT {

    private ExchangeApiRescript exchangeApi;

    @Before
    public void setup() {
        exchangeApi = new ExchangeApiRescript(config);
    }

    @Test
    public void shouldReturnEventTypes() {
        List<EventTypeResult> result = exchangeApi.listEventTypes(new MarketFilter());

        assertNotNull(result);
        assertFalse(result.isEmpty());
    }

}